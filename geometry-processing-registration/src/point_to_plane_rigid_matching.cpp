#include "point_to_plane_rigid_matching.h"
#include <Eigen/SVD>
#include <Eigen/LU>
#include <iostream>
#include "closest_rotation.h"

using namespace Eigen;

void point_to_plane_rigid_matching(
        const Eigen::MatrixXd &X,
        const Eigen::MatrixXd &P,
        const Eigen::MatrixXd &N,
        Eigen::Matrix3d &R,
        Eigen::RowVector3d &t) {
  R = Eigen::Matrix3d::Identity();
  t = Eigen::RowVector3d::Zero();
  Matrix3d R0;
  MatrixXd A;
  VectorXd zero = VectorXd::Zero(X.rows());
  VectorXd one = VectorXd::Ones(X.rows());
  VectorXd COL1 = X.col(0);
  VectorXd COL2 = X.col(1);
  VectorXd COL3 = X.col(2);

  A.resize(X.rows() * 3, 6);
  A << zero, COL3,-COL2, one, zero, zero,
      -COL3, zero, COL1, zero, one, zero,
       COL2,-COL1, zero, zero, zero, one;

  MatrixXd DiagN;
  DiagN.resize(X.rows(), X.rows() * 3);
  MatrixXd Diag_NX = N.col(0).asDiagonal();
  MatrixXd Diag_NY = N.col(1).asDiagonal();
  MatrixXd Diag_NZ = N.col(2).asDiagonal();
  DiagN << Diag_NX, Diag_NY, Diag_NZ;
  VectorXd Dists(X.rows() * 3);
  Dists << X.col(0) - P.col(0),
           X.col(1) - P.col(1),
           X.col(2) - P.col(2);
  A = DiagN * A;
  Dists =  DiagN * Dists;
  VectorXd u = (A.transpose() * A).inverse() * (-A.transpose() * Dists);
  double alpha, beta, gamma;
  alpha = u(0);
  beta = u(1);
  gamma = u(2);

  R0 <<    1, -gamma,   beta,
       gamma,      1, -alpha,
       -beta,  alpha,      1;
  t << u(3), u(4), u(5);
}

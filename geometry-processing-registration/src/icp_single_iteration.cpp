#include "icp_single_iteration.h"
#include "point_to_point_rigid_matching.h"
#include "hausdorff_lower_bound.h"
#include "random_points_on_mesh.h"
#include "point_mesh_distance.h"
#include "point_to_plane_rigid_matching.h"
#include <iostream>
// Conduct a single iteration of the iterative closest point method align
// (`VX`,`FX`) to (`VY`,`FY`) by finding the rigid transformation
// (`R`,`t`)minimizing the matching energy:
//
// \\[
//    ∫_X ‖ Rx+t - P_Y( Rx +t ) ‖² dA
// \\]
//
//
// Inputs:
//   VX  #VX by 3 list of mesh vertex positions
//   FX  #FX by 3 list of triangle mesh indices into VX
//   VY  #VY by 3 list of mesh vertex positions
//   FY  #FY by 3 list of triangle mesh indices into VY
//   num_samples  number of random samples to use (larger --> more accurate)
//   method  method of linearization to use, one ot ICP_METHOD_POINT_TO_POINT
//     or ICP_METHOD_POINT_TO_PLANE
// Outputs:
//   R  3 by 3 rotation matrix
//   t  3d translation vector


//icp V_X, F_X, V_Y, F_Y
//R,t ← initialize (e.g., set to identity transformation)
//repeat until convergence -> But not here :)
//        X ← sample source mesh (V_X,F_X)
//P0 ← project all X onto target mesh (V_Y,F_Y)
//R,t ← update rigid transform to best match X and P0
//V_X ← rigidly transform original source mesh by R and t

using namespace Eigen;
void icp_single_iteration(
  const Eigen::MatrixXd & VX,
  const Eigen::MatrixXi & FX,
  const Eigen::MatrixXd & VY,
  const Eigen::MatrixXi & FY,
  const int num_samples,
  const ICPMethod method,
  Eigen::Matrix3d & R,
  Eigen::RowVector3d & t)
{
  // Sample X
  Eigen::MatrixXd X;
  random_points_on_mesh(num_samples, VX, FX, X);

  // Compute P
  Eigen::MatrixXd P, N;
  Eigen::VectorXd D;
  point_mesh_distance(X, VY, FY, D, P, N);

  if (method == ICP_METHOD_POINT_TO_POINT)
    point_to_point_rigid_matching(X, P, R, t);
  else
    point_to_plane_rigid_matching(X, P, N, R, t);

}

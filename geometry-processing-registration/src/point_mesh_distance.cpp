// Compute the distances `D` between a set of given points `X` and their
// closest points `P` on a given mesh with vertex positions `VY` and face
// indices `FY`.
//
// Inputs:
//   X  #X by 3 list of query positions
//   VY  #VY by 3 list of mesh vertex positions
//   FY  #FY by 3 list of triangle mesh indices into VY
// Outputs:
//   D  #X list of distances from X to P
//   P  #X by 3 list of 3d position of closest points
//   N  #X by 3 list of 3d unit normal vectors of closest points
#include "point_mesh_distance.h"
#include "point_triangle_distance.h"
#include <igl/per_face_normals.h>
#include <Eigen/Geometry>
#include <iostream>
#include <algorithm>
#include <limits>

using namespace Eigen;
void point_mesh_distance(const Eigen::MatrixXd &X,
                         const Eigen::MatrixXd &VY,
                         const Eigen::MatrixXi &FY,
                         Eigen::VectorXd &D,
                         Eigen::MatrixXd &P,
                         Eigen::MatrixXd &N)
{
  int MAX_SIZE = 10000000;
  double MAX_DOUBLE = 10000000000.;
  std::vector<double> dists(MAX_SIZE);
  std::vector<RowVector3d, aligned_allocator<RowVector3d>> dists_p(MAX_SIZE);
  double d, min_d;
  RowVector3d p, proj_p, proj_n;
  MatrixXd TMPN;
  TMPN.resizeLike(X);
  P.resizeLike(X);
  N.resizeLike(X);
  D.conservativeResize(X.rows());
  igl::per_face_normals(VY, FY, Vector3d(0., 0., 0.), TMPN);
  for (int pt = 0; pt < X.rows();  ++pt) {
    min_d = MAX_DOUBLE;
    for (int i = 0; i < FY.rows(); ++i) {
      RowVector3i ii = FY.row(i);
      point_triangle_distance(X.row(pt), VY.row(ii(0)), VY.row(ii(1)), VY.row(ii(2)), d, p);
      if (d < min_d) {
        min_d  = d;
        proj_p = p;
        proj_n = TMPN.row(i);
      }
    }
    D(pt)     =  min_d;
    P.row(pt) = proj_p;
    N.row(pt) = proj_n;
}

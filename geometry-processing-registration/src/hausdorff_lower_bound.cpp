#include "hausdorff_lower_bound.h"
#include "random_points_on_mesh.h"
#include "point_mesh_distance.h"
// Compute a lower bound on the _directed_ Hausdorff distance from a given mesh
// (VX,FX) to another mesh (VY,FY).
//
//
// Inputs:

//   VX  #VX by 3 list of mesh vertex positions
//   FX   #FX by 3 list of triangle mesh indices into VX
//   VY  #VY by 3 list of mesh vertex positions
//   FY  #FY by 3 list of triangle mesh indices into VY
//   n  number of random samples to use (larger --> more accurate)
// Returns lower bound
using namespace Eigen;
using namespace std;
double hausdorff_lower_bound(
  const Eigen::MatrixXd & VX,
  const Eigen::MatrixXi & FX,
  const Eigen::MatrixXd & VY,
  const Eigen::MatrixXi & FY,
  const int n)
{
  MatrixXd X, P, N;
  VectorXd D;
  random_points_on_mesh(n, VX, FX, X);
  point_mesh_distance(X, VY, FY, D, P, N);

  return D.maxCoeff();
}

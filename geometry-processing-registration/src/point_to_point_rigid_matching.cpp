#include "point_to_point_rigid_matching.h"
#include <Eigen/SVD>
#include <Eigen/LU>
#include <iostream>
#include "closest_rotation.h"
//#include <igl/polar_svd.h>
// Given a set of source points X and corresponding target points P, find the
// optimal rigid transformation (R,t) that aligns X to P, minimizing the
// matching energy:
//
//   ‖ R X' - t' 1' - P' ‖²
//
// Inputs:
//   X  #X by 3 set of source points
//   P  #X by 3 set of target points
// Outputs:
//   R  3 by 3 rotation matrix
//   t  3d translation vector
//
using namespace Eigen;

void point_to_point_rigid_matching(
        const Eigen::MatrixXd &X,
        const Eigen::MatrixXd &P,
        Eigen::Matrix3d &R,
        Eigen::RowVector3d &t) {
  // Replace with your code
  Vector3d centroid_x = X.colwise().mean();
  Vector3d centroid_p = P.colwise().mean();

  MatrixXd X2 = (X.rowwise() - centroid_x.transpose()).eval();
  MatrixXd P2 = (P.rowwise() - centroid_p.transpose()).eval();

  closest_rotation(P2.transpose() * X2, R);

  t = centroid_p - R * centroid_x;

}


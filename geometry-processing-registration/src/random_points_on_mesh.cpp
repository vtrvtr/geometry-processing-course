#include "random_points_on_mesh.h"
#include <iostream>
#include <igl/cumsum.h>
#include <igl/doublearea.h>

using namespace Eigen;
using namespace std;

inline const double random_double() {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<double> dist(0.0, 1.0);
  return dist(mt);
}

RowVector3i get_random_face(const MatrixXd &CS, const MatrixXi &F) {
  auto guess = lower_bound(CS.array().data(), CS.array().data() + CS.rows(), random_double());
  return F.row(distance(CS.array().data(), guess));
}

void random_points_on_mesh(
        const int n,
        const Eigen::MatrixXd &V,
        const Eigen::MatrixXi &F,
        Eigen::MatrixXd &X) {
  // REPLACE WITH YOUR CODE:
//  X.resize(n,3); for(int i = 0;i<X.rows();i++) X.row(i) = V.row(i%V.rows());
//  double alpha, beta, gamma;
  auto some_point = [&V](RowVector3i F, double alpha, double beta) -> RowVector3d {
      auto p1 = V.row(F(0));
      auto p2 = V.row(F(1));
      auto p3 = V.row(F(2));
      return p1 + alpha * (p2 - p1) + beta * (p3 - p1);
  };

  MatrixXd Areas(F.rows(), 3);
  MatrixXd RelativeAreas;
  MatrixXd CS;
  igl::doublearea(V, F, Areas);
  const double total_area = Areas.sum();
  RelativeAreas = Areas / total_area;
  igl::cumsum(RelativeAreas, 1, CS);

  X.resize(n, 3);
  for (int row = 0; row < n; ++row) {
    X.row(row) = some_point(get_random_face(CS, F), 1 - random_double(), 1 - random_double());
  }


}


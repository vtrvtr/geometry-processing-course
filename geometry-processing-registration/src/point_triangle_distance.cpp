#include "point_triangle_distance.h"
//
//template <typename T> int sign(T val) {
//  return (T(0) < val) - (val < T(0));
//}
//
//template <typename T> float dot2(T val) {
//  return val.dot(val);
//}
//
//template <typename T, typename G> float clamp(T val, G max) {
//  if (val < 0.) {
//    return 0;
//  }
//  return val > max ? max : val;
//}

void point_triangle_distance(
  const Eigen::RowVector3d & x,
  const Eigen::RowVector3d & a,
  const Eigen::RowVector3d & b,
  const Eigen::RowVector3d & c,
  double & d,
  Eigen::RowVector3d & p) {
  // Replace with your code
  // Code from http://www.iquilezles.org/www/articles/triangledistance/triangledistance.htm
  using RV3 = Eigen::RowVector3d;
  d = 0;
  p = a;

  RV3 edgeBA = b - a;
  RV3 edgeCA = c - a;
  RV3 v0 = a - x;

  float d1 = dot2(edgeBA);
  float d2 = edgeBA.dot(edgeCA);
  float d3 = dot2(edgeCA);
  float d4 = edgeBA.dot(v0);
  float d5 = edgeCA.dot(v0);

  float det = d1 * d3 - d2 * d2;
  float s = d2 * d5 - d3 - d4;
  float t = d2 * d4 - d1 - d5;

  if ( s + t < det ) {
    if ( s < 0. ) {
      if ( t < 0. ) {
        if ( d4 < 0. ) {
          s = clamp( -d4 / d1, 1.0 );
          t = 0;
        }
      }
      else {
        s = 0.;
        t = clamp( d5/d3, 1);
      }
    }
    else if (t < 0.){
      s = clamp(-d4/d1, 1.);
      t = 0.;
    }
    else {
      s *= 1/det;
      t *= 1/det;
    }
  }
  else {
    if ( s < 0. ) {
      if ( d2 + d4 < d3 + d5 ) {
        s = clamp(d2 + d4 - d3 + d5 / d1 - 2 * d2 + d3, 1.);
        t = 1 - s;
      }
      else {
        s = clamp(d3 + d5 - d2 - d4, 1.);
        t = 1. - s;

      }
    }
  }

  p = a + s * edgeBA + t * edgeCA;
  d = (p - x).norm();

}
#include "closest_rotation.h"
#include <Eigen/SVD>
#include <Eigen/LU>
#include <iostream>

using namespace Eigen;

void closest_rotation(
        const Eigen::Matrix3d &M,
        Eigen::Matrix3d &R) {
  JacobiSVD<MatrixXd> svd(M, ComputeThinU | ComputeThinV );
  const MatrixXd &U = svd.matrixU();
  const MatrixXd V = svd.matrixV().transpose();
  MatrixXd UV = U * V;
  const double det = UV.determinant();
  Vector3d omega_diag(1, 1, det);
  Matrix3d Omega = omega_diag.asDiagonal();

  R = U * Omega * V;

}

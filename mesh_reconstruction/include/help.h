//
// Created by vhugo on 5/22/19.
//

#ifndef MESH_RECONSTRUCTION_HELP_H
#define MESH_RECONSTRUCTION_HELP_H
#include <iostream>
#include <Eigen/Sparse>
#include <vector>


namespace helpers {
    using S = Eigen::SparseMatrix<double>;
    using Tp = Eigen::Triplet<double>;

    template<typename G>
    G concatenate(const G &M1, const G &M2) {
      std::vector<Tp> triplets;
      const int max_rows = M1.rows() + M2.rows();
      triplets.reserve(max_rows);
      S D(max_rows, M1.cols());

      for (int i = 0; i < M1.outerSize(); ++i) {
        for (S::InnerIterator it(M1, i); it; ++it) {
          triplets.emplace_back(Tp({it.row(), it.col(), it.value()}));
        }
      }

      for (int j = 0; j < M2.outerSize(); ++j) {
        for (S::InnerIterator it2(M2, j); it2; ++it2) {
          triplets.emplace_back(Tp({it2.row() + M1.rows(), it2.col(), it2.value()}));
        }
      }

      D.setFromTriplets(triplets.cbegin(), triplets.cend());

      return D;
    }

    template<typename G, typename... Args>
    G concatenate(const G &M1, const G &M2, const Args &... Mn) {
      auto TMP = concatenate(M1, M2);
      return concatenate(TMP, Mn...);

    }
}

#endif //MESH_RECONSTRUCTION_HELP_H

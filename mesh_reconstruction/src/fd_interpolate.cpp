#include "fd_interpolate.h"
#include <iostream>
#include <igl/floor.h>

using Tp = Eigen::Triplet<double>;
using VecD = Eigen::RowVector3d;
using VecI = Eigen::RowVector3i;

void fd_interpolate(
        const int nx,
        const int ny,
        const int nz,
        const double h,
        const Eigen::RowVector3d &corner,
        const Eigen::MatrixXd &P,
        Eigen::SparseMatrix<double> &W) {
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  ////////////////////////////////////////////////////////////////////////////
  std::vector<Tp, Eigen::aligned_allocator<Tp>> triplets;

  for (int row = 0; row < P.rows(); ++row) {
    VecD pos = P.row(row);
    VecD rel_pos = (pos - corner) / h;

    VecD box_id;
    igl::floor(rel_pos, box_id);

    VecD dist = rel_pos - box_id;


    auto get_index = [&nx, &ny, &box_id](int x, int y, int z) -> int {
        return (box_id(0) + x) + (nx * (box_id(1) + y)) + (nx * ny * (z + box_id(2)));
    };
    // Following this picture here:
    // https://en.wikipedia.org/wiki/Trilinear_interpolation#/media/File:Enclosing_points.svg
    triplets.emplace_back(Tp({row, get_index(0, 0, 0),
                              1 - dist(0) * 1 - dist(1) * 1 - dist(2)}));
    triplets.emplace_back(Tp({row, get_index(1, 0, 0),
                              dist(0) * (1 - dist(1)) * (1 - dist(2))}));
    triplets.emplace_back(Tp({row, get_index(0, 1, 0),
                              1 - dist(0) * dist(1) * 1 - dist(2)}));
    triplets.emplace_back(Tp({row, get_index(1, 1, 0),
                              dist(0) * dist(1) * 1 - dist(2)}));
    triplets.emplace_back(Tp({row, get_index(0, 0, 1),
                              1 - dist(0) * 1 - dist(1) * dist(2)}));
    triplets.emplace_back(Tp({row, get_index(1, 1, 0),
                              dist(0) * dist(1) * 1 - dist(2)}));
    triplets.emplace_back(Tp({row, get_index(0, 1, 1),
                              1 - dist(0) * dist(1) * dist(2)}));
    triplets.emplace_back(Tp({row, get_index(1, 1, 1),
                              dist(0) * dist(1) * dist(2)}));

  }
  W.resize(P.rows(), nx * ny * nz);
  W.setFromTriplets(triplets.begin(), triplets.end());

}


#include "fd_partial_derivative.h"
#include <iostream>
// See also: fd_partial_derivative.h
using Tp = Eigen::Triplet<double>;
using VecD = Eigen::RowVector3d;
using VecI = Eigen::RowVector3i;

void fd_partial_derivative(
        const int nx,
        const int ny,
        const int nz,
        const double h,
        const int dir,
        Eigen::SparseMatrix<double> &D) {
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  ////////////////////////////////////////////////////////////////////////////
  const int COLS = nx * ny * nz;
  int X_MAX, Y_MAX, Z_MAX, offset_x, offset_y, offset_z;
  std::vector<Tp, Eigen::aligned_allocator<Tp>> triplets;
  switch (dir) {
    case 0:
      X_MAX = nx - 1;
      Y_MAX = ny;
      Z_MAX = nz;
      offset_x = 1;
      offset_y = 0;
      offset_z = 0;
      break;

    case 1:
      X_MAX = nx;
      Y_MAX = ny - 1;
      Z_MAX = nz;

      offset_x = 0;
      offset_y = 1;
      offset_z = 0;
      break;

    case 2:
      X_MAX = nx;
      Y_MAX = ny;
      Z_MAX = nz - 1;

      offset_x = 0;
      offset_y = 0;
      offset_z = 1;
      break;
  };
  triplets.reserve(X_MAX * Y_MAX * Z_MAX);
  for (int x = 0; x < X_MAX; ++x) {
    for (int y = 0; y < Y_MAX; ++y) {
      for (int z = 0; z < Z_MAX; ++z) {
        auto get_index = [](int x, int y, int z, int nx, int ny) -> int { return x + (nx * y) + (nx * ny * z); };

        triplets.emplace_back(Tp({get_index(x, y, z, X_MAX, Y_MAX),
                                  get_index(x, y, z, nx, ny),
                                  -1}));
        triplets.emplace_back(Tp({get_index(x, y, z, X_MAX, Y_MAX),
                                  get_index(x + offset_x, y + offset_y, z + offset_z, nx, ny),
                                  1}));
      }
    }
  }
  D.resize(X_MAX * Y_MAX * Z_MAX, COLS);
  D.setFromTriplets(triplets.begin(), triplets.end()); //

}

#include "fd_grad.h"
#include "fd_partial_derivative.h"
#include <iostream>
#include "help.h"
// Construct a gradient matrix for a finite-difference grid

//
// Inputs:
//   nx  number of grid steps along the x-direction
//   ny  number of grid steps along the y-direction
//   nz  number of grid steps along the z-direction
//   h  grid step size
// Outputs:
//   G  (nx-1)*ny*nz+ nx*(ny-1)*nz+ nx*ny*(nz-1) by nx*ny*nz sparse gradient
//     matrix: G = [Dx;Dy;Dz]
//
void fd_grad(
        const int nx,
        const int ny,
        const int nz,
        const double h,
        Eigen::SparseMatrix<double> &G) {
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  ////////////////////////////////////////////////////////////////////////////

  Eigen::SparseMatrix<double> Dx, Dy, Dz;
  fd_partial_derivative(nx, ny, nz, h, 0, Dx);
  fd_partial_derivative(nx, ny, nz, h, 1, Dy);
  fd_partial_derivative(nx, ny, nz, h, 2, Dz);

  G = helpers::concatenate(Dx, Dy, Dz);
}

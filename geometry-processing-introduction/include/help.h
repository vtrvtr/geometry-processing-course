#ifndef INTRODUCTION_HELPERS_H
#define INTRODUCTION_HELPERS_H

#include <Eigen/Core>
#include <iostream>
#include <set>

namespace helpers {
    using Sint = std::set<int>;
    using a_row = Eigen::Matrix<int, 1, 3>;
    using single_edge_matrix = Eigen::Matrix<int, 3, 2>;
    using vp = std::vector<std::pair<int, int>>;

    template<typename T>
    typename std::enable_if<std::is_integral<T>::value>::type
    print(const T &s) {
      std::cout << s << std::endl;
    }

    template<typename T>
    typename std::enable_if<!std::is_integral<T>::value>::type
    print(const T &s) {
      for (auto i : s) {
        std::cout << i << std::endl;
      }
    }

    void print(const char *s);

    template<typename T>
    struct LessPredicate {
        bool operator()(const std::pair <T, T> &lhs, const std::pair <T, T> &rhs) const {
          const auto lhs_order = lhs.first < lhs.second ? lhs : std::tie(lhs.second, lhs.first);
          const auto rhs_order = rhs.first < rhs.second ? rhs : std::tie(rhs.second, lhs.first);
          return lhs_order < rhs_order;
        }
    };

    template<typename T>
    std::vector<int> get_range(const T t) {

      std::vector<int> range(t);
      std::iota(range.begin(), range.end(), 0);
      return range;

    }

    template<typename DerivedF>
    Sint get_vertices(DerivedF F2) {
      Sint tmp;
      for (int row = 0; row < F2.rows(); ++row) {
        for (int col = 0; col < F2.cols(); ++col) {
          tmp.insert(F2(row, col));
        }
      }
      return tmp;
    };

    single_edge_matrix half_edges_m(a_row &r);

    vp half_edges_v(a_row &&r);

    bool already_in(const vp &v, const std::pair<int, int> p);

}
#endif //INTRODUCTION_HELPERS_H

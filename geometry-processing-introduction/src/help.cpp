#include "help.h"

namespace helpers {

    single_edge_matrix half_edges_m(a_row &&r) {
      int first, second, third;
      first = r(0, 0);
      second = r(0, 1);
      third = r(0, 2);
      single_edge_matrix _E;
      _E << first, second,
              second, third,
              third, first;
      return _E;
    }

    vp half_edges_v(a_row &&r) {
      int first, second, third;
      first = r(0, 0);
      second = r(0, 1);
      third = r(0, 2);
      std::vector<std::pair<int, int>> v;
      v.emplace_back(std::make_pair(first, second));
      v.emplace_back(std::make_pair(second, third));
      v.emplace_back(std::make_pair(third, first));
      return v;
    }

    bool already_in(const vp &v, const std::pair<int, int> p) {
      if (std::find(v.begin(), v.end(), p) != v.end()) {
        return true;
      }
      auto reversed = std::pair<int, int>(p.second, p.first);

      return std::find(v.begin(), v.end(), reversed) != v.end();
    }

    void print(const char *s) {
      std::cout << s << std::endl;
    }
}



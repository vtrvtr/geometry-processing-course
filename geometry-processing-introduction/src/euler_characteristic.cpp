#include "euler_characteristic.h"
#include "edges.h"
#include "help.h"
#include <set>
#include <iostream>
#include <Eigen/SparseCore>

int euler_characteristic(const Eigen::MatrixXi &F)
{
  int Chi = 0;
  auto num_edges = edges(F).rows();
  auto num_faces = F.rows();
  auto num_vertices = helpers::get_vertices(F).size();

  helpers::print(num_edges);
  helpers::print(num_faces);
  helpers::print(num_vertices);

  return num_vertices - num_edges + num_faces;
}


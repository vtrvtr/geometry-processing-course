#include "edges.h"
#include <iostream>
#include "help.h"

Eigen::MatrixXi edges(const Eigen::MatrixXi &M) {
  Eigen::MatrixXi E;
  std::vector<std::pair<int, int>> tmp;
  const static int edge_matrix_size = 2;
  const static long int row_size = M.rows();
  const static long int col_size = M.cols();

  std::vector<int> row_range = helpers::get_range(row_size);

  for (auto i : row_range) {
    for (auto vec : helpers::half_edges_v(M.row(i))) {
      if (!helpers::already_in(tmp, vec)) {
        tmp.emplace_back(vec);
      }
    }
  }

  E.resize(tmp.size(), edge_matrix_size);

  std::vector<int> tmp_range = helpers::get_range(tmp.size());
  for (auto tmp_row : tmp_range) {
      auto p = tmp[tmp_row];
      E(tmp_row, 0) = p.first;
      E(tmp_row, 1) = p.second;
  }
  return E;
}


#include "edges.h"
#include "help.h"
#include "euler_characteristic.h"
#include <igl/read_triangle_mesh.h>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/edges.h>
#include <igl/euler_characteristic.h>

int main(int argc, char *argv[]) {
  Eigen::MatrixXd V;
  Eigen::MatrixXi F;
  Eigen::MatrixXi ActualE;
  // Load in a mesh
  igl::read_triangle_mesh(argc > 1 ? argv[1] : "../shared/data/bunny.off", V, F);
  igl::edges(F, ActualE);


  Eigen::MatrixXi E = edges(F);
  int Chi = euler_characteristic(F);
  std::cout << "Edge list E is " << E.rows() << "x" << E.cols() << std::endl;
  std::cout << "Euler Characteristic: " << Chi << std::endl;
  std::cout << "Actual Edge list E is " << ActualE.rows() << "x" << ActualE.cols() << std::endl;
  helpers::print(igl::euler_characteristic(F));

  // Create a libigl Viewer object 
  igl::opengl::glfw::Viewer viewer;
// Set the vertices and faces for the viewer
  viewer.data().set_mesh(V, F);
// Launch a viewer instance
  viewer.launch();
  return 0;
}

